
InkWell(
  onTap: () async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LoginWidget(),
      ),
    );
  },
  child: Container(
    width: 150,
    height: 130,
    child: RiveAnimation.network(
      'https://public.rive.app/community/runtime-files/1199-2317-jack-olantern.riv',
      artboard: 'New Artboard',
      fit: BoxFit.cover,
      controllers: riveAnimationControllers,
    ),
  ),
)
